/*global require*/
// Node modules
var fs = require('fs'), vm = require('vm'), merge = require('deeply'), chalk = require('chalk'), es = require('event-stream');

// Gulp and plugins
var gulp = require('gulp'), rjs = require('gulp-requirejs-bundler'), concat = require('gulp-concat'), clean = require('gulp-clean'),
    replace = require('gulp-replace'), uglify = require('gulp-uglify'), htmlreplace = require('gulp-html-replace');

var jsdoc = require("gulp-jsdoc");
var preprocess = require('gulp-preprocess');

// Config
var requireJsRuntimeConfig = vm.runInNewContext(fs.readFileSync('src/app/require.config.js') + '; require;');

var requireJsOptimizerConfig = merge(requireJsRuntimeConfig, {
    out: 'scripts.js',
    baseUrl: './src',
    name: 'app/startup',
    paths: {
        requireLib: 'bower_modules/requirejs/require'
    },
    include: [
        'requireLib',
        'components/nav-bar/nav-bar',
        'components/home-page/home',

        'components/about-page/about',
        'text!components/about-page/about.html',

        'components/debug-page/debug-page',
        'text!components/debug-page/debug-page.html',

        'components/results-page/results-page',
        'text!components/results-page/results-page.html',

        'components/series-page/series-page',
        'text!components/series-page/series-page.html',

        'components/suite-page/suite-page',
        'text!components/suite-page/suite-page.html',

        'components/sweep-page/sweep-page',
        'text!components/sweep-page/sweep-page.html',

        'components/sequence-page/sequence-page',
        'text!components/sequence-page/sequence-page.html'
    ],
    insertRequire: ['app/startup'],
    bundles: {
        // If you want parts of the site to load on demand, remove them from the 'include' list
        // above, and group them into bundles here.
        // 'bundle-name': [ 'some/module', 'another/module' ],
        // 'another-bundle-name': [ 'yet-another-module' ]
    }
});

// Added JSDoc task. (Frank 18Sep2014)
gulp.task('jsdoc', function () {
    return gulp.src(['./src/*.js', 'src/services/*.js', 'src/components/**/*.js', 'src/app/*.js', 'README.md'])
    .pipe(jsdoc('./src/jsdoc'));
});

// Discovers all AMD dependencies, concatenates together all required .js files, preprocesses, minifies them
// Added preprocess to the js task. (Frank 18Sep2014)

gulp.task('js', function () {
    return rjs(requireJsOptimizerConfig)
        .pipe(preprocess())
        .pipe(uglify({ preserveComments: 'some' }))
        .pipe(gulp.dest('./dist/'));
});

// Concatenates CSS files, rewrites relative paths to Bootstrap fonts, copies Bootstrap fonts
gulp.task('css', function () {
    var bowerCss = gulp.src('src/bower_modules/components-bootstrap/css/bootstrap.min.css')
            .pipe(replace(/url\((')?\.\.\/fonts\//g, 'url($1fonts/')),
        appCss = gulp.src('src/css/*.css'),
        toastrCss = gulp.src('src/bower_modules/toastr/*.css'),
        combinedCss = es.concat(bowerCss, appCss, toastrCss).pipe(concat('css.css')),
        fontFiles = gulp.src('./src/bower_modules/components-bootstrap/fonts/*', { base: './src/bower_modules/components-bootstrap/' });
    return es.concat(combinedCss, fontFiles)
        .pipe(gulp.dest('./dist/'));
});

// Copies index.html, replacing <script> and <link> tags to reference production URLs
gulp.task('html', function () {
    return gulp.src('./src/index.html')
        .pipe(htmlreplace({
            'css': 'css.css',
            'js': 'scripts.js'
        }))
        .pipe(gulp.dest('./dist/'));
});

// Removes all files from ./dist/
gulp.task('clean', function () {
    return gulp.src('./dist/**/*', { read: false })
        .pipe(clean());
});

gulp.task('default', ['html', 'js', 'css'], function (callback) {
    callback();
    console.log('\nPlaced optimized files in ' + chalk.magenta('dist/\n'));
});
