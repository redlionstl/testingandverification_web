//----------------------------------------------------------------------------
// Copyright(c) 2014 Red Lion. All rights reserved.
//----------------------------------------------------------------------------
//
//   Creator: Frank Spafford
//   Created: 8/20/2014  14:39
//
// Description:
// ------------
// View model for the Suite page.
//----------------------------------------------------------------------------

/**
 This is the JSDoc namespace used for the suite view.
 @namespace suite
*/

// http://lint-explain.herokuapp.com/a-is-defined-but-never-used/
/*jshint unused: vars */


define(['knockout', 'text!./suite-page.html', 'data', 'lib'], function (ko, templateMarkup, data, lib) {

    "use strict"; // Defines that JavaScript code should be executed in "strict mode".

    /**
     Suite view model
     @class SuitePage
     @memberOf suite
     @todo Cleanup proof-of-concept code.
     @todo Read JSON files based on known file and folder names.
     @todo Update JSON field names.
    */
    function SuitePage(params) {
        var callbackFail;
        var callbackSuccess;
        var self;
        var suite;

        self = this;

        // TODO: Read suiteResults.json (Frank 15Sep2014)

        // Start with the data saved by the previous page, the row data for the drill down.
        suite = data.getData().suite;

        self.declareProperties(suite);

        // TODO: Proof-of-concept fake data (will be disabled or removed): (Frank 25Sep2014)
        self.seriesData = [
        {
            "name": "SeriesOne",
            "description": {
                "summary": "Verifies that the a settings string can be changed via the browser",
                "detailed": "This series validates that the system can log on to a device via a web browser, change a text field, validate that change then log off"
            },
            "definition": "somepath/testseries.json",// TODO:  (Frank 15Sep2014)
            "status": "booboo",// TODO:  (Frank 15Sep2014)
            "statusStyle": "warning",// TODO:  (Frank 15Sep2014)
            "ports": "1,2",// TODO:  (Frank 15Sep2014)
            "configuration": "The device should be in factory defaults for this series.",
            "startTimestamp": "8/13/2015 13:02:45",
            "endTimestamp": "9/23/2015 13:05:12",
        },
        {
            "name": "SeriesTwo",
            "description": {
                "summary": "Verifies that the a settings string can be changed via the browser",
                "detailed": "This series validates that the system can log on to a device via a web browser, change a text field, validate that change then log off"
            },
            "definition": "somepath/testseries.json",
            "status": "booboo2",
            "statusStyle": "warning",
            "ports": "1,2",
            "configuration": "The device should be facing north for this series.",
            "startTimestamp": "7/13/2010 13:02:45",
            "endTimestamp": "9/23/2017 13:05:12",
        }
        ];

        self.modifyRowData(self.seriesData);

        self.availableRows = ko.observableArray(self.seriesData);

        // Create bounded wrappers for the callbacks so they execute in the scope of this (self) object.

        callbackSuccess = lib.createBoundedWrapper(this, this.getJSON);
        callbackFail = lib.createBoundedWrapper(this, this.getJSONFailed);

        // TODO: Read JSON file based on known file/folder names. (Frank 29Sep2014)
        lib.getJSON(self.definition(), callbackSuccess, callbackFail);
    }

    /**
     Drill down (show the details for) the selected row.
     @function goTo
     @memberOf suite.SuitePage
     @param row The data for the selected row.
    */
    SuitePage.prototype.goTo = function (row) {
        data.getData().series = row;

        //console.log(data.getData().series);

        location.hash = "series";
    };

    // This runs when the component is torn down. Put here any logic necessary to clean up,
    // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
    SuitePage.prototype.dispose = function () { };

    /**
     Declare the observable properties for the page.
     @function declareProperties
     @memberOf suite.SuitePage
     @todo Finish implementation.
     */
    SuitePage.prototype.declareProperties = function (data) {
        try {
            var self;

            self = this;
            self.name = ko.observable(data.name);
            self.summary = ko.observable(data.description.summary);
            self.description = ko.observable(data.description.detailed);
            self.definition = ko.observable(data.definition);
            self.endDate = ko.observable(data.endTimestamp);
            self.startDate = ko.observable(data.startTimestamp);
            self.status = ko.observable(data.status);

            self.theory = ko.observable(data.theory);
            self.instructions = ko.observable(data.setup.instructions);

            self.unknown = "(not yet implemented)";

            self.statusStyle = ko.observable("text-" + lib.getStatusStyle(data.status));
        } catch (e) {
            logger.logError('Exception: ' + e.message, e.lineNumber, e.fileNamesource, true);
        }
    };

    /**
     Update the observable properties for the page.
     @function updateProperties
     @memberOf suite.SuitePage
     @todo Finish implementation.
     */
    SuitePage.prototype.updateProperties = function (data) {
        try {
            var self;

            self = this;
            self.name(data.name);
            self.summary(data.description.summary);
            self.description(data.description.detailed);
            self.definition(data.definition);
            self.endDate(data.endTimestamp);
            self.startDate(data.startTimestamp);
            self.status(data.status);

            self.theory(data.theory);
            self.instructions(data.setup.instructions);

            self.statusStyle("text-" + lib.getStatusStyle(data.status));
        } catch (e) {
            logger.logError('Exception: ' + e.message, e.lineNumber, e.fileNamesource, true);
        }
    };

    /**
     Modify the row (grid) data shown on the page.
     @function modifyRowData
     @memberOf suite.SuitePage
     @param rowData Data associated with a row of the grid.
     @todo Finish implementation.
     */
    SuitePage.prototype.modifyRowData = function (rowData) {
        var i, style;

        // TODO: Read status from results file (Frank 18Aug2014)

        for (i = 0; i < rowData.length; i++) {
            if (i % 2 === 0) {
                rowData[i].status = 'pass';
            } else {
                rowData[i].status = 'fail';
            }

            // Use a CSS style to color the status field.

            style = lib.getStatusStyle(rowData[i].status);
            rowData[i].statusStyle = style;

            rowData[i].config = "a config port";
            rowData[i].ports = "a port set";
        }
    };

    /**
     Handle suite and the contained series(s) data.
     @function getJSON
     @memberOf suite.SuitePage
     @param data - data read from JSON file.
     @todo Finish implementation.
    */
    SuitePage.prototype.getJSON = function (data) {
        try {
            var self;

            self = this;

            //console.log(data.suiteInfo);

            self.updateProperties(data.suiteInfo);

            self.modifyRowData(data.series);

            // Update the model so the view is updated.

            self.availableRows(data.series);

        } catch (e) {
            logger.logError(e.message, e.lineNumber, e.fileNamesource, true);
        }
    };

    /**
     Handle error when reading suite and the contained series(s) data.
     @function getJSONFailed
     @memberOf suite.SuitePage
     @param errorThrown - description of the error.
     @todo Finish implementation.
    */
    SuitePage.prototype.getJSONFailed = function (errorThrown) {
        try {
            var self;

            self = this;

            console.log(errorThrown);
            // TODO: enable later (Frank 05Sep2014)
            //self.availableRows.removeAll();

        } catch (e) {
            logger.logError(e.message, e.lineNumber, e.fileNamesource, true);
        }
    };

    return { viewModel: SuitePage, template: templateMarkup };

});
