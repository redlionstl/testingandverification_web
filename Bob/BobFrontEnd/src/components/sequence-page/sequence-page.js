//----------------------------------------------------------------------------
// Copyright(c) 2014 Red Lion. All rights reserved.
//----------------------------------------------------------------------------
//
//   Creator: Frank Spafford
//   Created: 8/18/2014
//
// Description:
// ------------
// This sequence-page shows the list of test steps that have been run.
//----------------------------------------------------------------------------

/**
 * This is the JSDoc namespace used for the sequence view.
 * @namespace sequence
 */

// http://lint-explain.herokuapp.com/a-is-defined-but-never-used/
/*jshint unused: vars */


define(['knockout', 'text!./sequence-page.html', 'data', 'lib'], function (ko, templateMarkup, data, lib) {

    "use strict"; // Defines that JavaScript code should be executed in "strict mode".

    /**
     * Test results view model
     * @class SequencePage
     * @memberOf sequence
     * @todo Cleanup proof-of-concept code.
     * @todo Read JSON files based on known file and folder names.
     */
    function SequencePage(params) {
        var callbackFail;
        var callbackSuccess;
        var self;
        var sequence;

        self = this;

        // TODO: Read sequenceConfig.json and sequenceResults.json (Frank 15Sep2014)

        // Start with the data saved by the previous page, the row data for the drill down.

        sequence = data.getData().sequence;
        console.log(sequence);

        self.declareProperties(sequence);

        // TODO: Proof-of-concept fake data (will be disabled or removed): (Frank 25Sep2014)
        //self.resultsData = [
        //{
        //    "name": "port1rxsigframe",
        //    "value": 24,
        //    "status": "boo"
        //},
        //{
        //    "name": "port2rxsigframe",
        //    "value": 103,
        //    "status": "boo2"
        //},
        //{
        //    "name": "port22txsigframe",
        //    "value": 3,
        //    "status": "boo2"
        //},
        //{
        //    "name": "port123txsigframe",
        //    "value": 123456.45,
        //    "status": "boo2"
        //}
        //];

        // TODO: Proof-of-concept fake data (will be disabled or removed): (Frank 25Sep2014)
        self.resultsData = {
            "sequenceConfig": {
                "name": "Change Device Location",
                "description": {
                    "summary": "Verifies changing of the device location string",
                    "detailed": "This sequence validates that the system can log on to a device via a web browser, change a text field, validate that change then log off"
                },
                "stepList": [{
                    "Action": "Log on to the device",
                    "Validation": "Was log on successful",
                    "valueList": [
                    {
                        "name": "returnValue",
                        "deviceList": [{
                            "deviceType": "default",
                            "formatList": [{
                                "operand": "Success",
                                "operator": "isExact",
                                "result": "Pass"
                            },
                            {
                                "operand": "Success",
                                "operator": "isNotExact",
                                "result": "Fail"
                            }]
                        }]
                    },
                    {
                        "name": "returnValue2",
                        "deviceList": [{
                            "deviceType": "default",
                            "formatList": [{
                                "operand": "Success",
                                "operator": "isExact",
                                "result": "Pass"
                            },
                            {
                                "operand": "Success",
                                "operator": "isNotExact",
                                "result": "Fail"
                            }]
                        }]
                    }
                    ]
                },
                {
                    "Action": "Change device location and save config",
                    "Validation": "Was device location saved",
                    "valueList": [{
                        "name": "returnValue3",
                        "deviceList": [{
                            "deviceType": "default",
                            "formatList": [{
                                "operand": "Success",
                                "operator": "isExact",
                                "result": "Pass"
                            },
                            {
                                "operand": "Success",
                                "operator": "isNotExact",
                                "result": "Fail"
                            }]
                        }]
                    }]
                },
                {
                    "Action": "Log out of device",
                    "Validation": "Was log out successful",
                    "valueList": [{
                        "name": "returnValue4",
                        "deviceList": [{
                            "deviceType": "default",
                            "format": [{
                                "operand": "Success",
                                "operator": "isExact",
                                "result": "Pass"
                            },
                            {
                                "operand": "Success",
                                "operator": "isNotExact",
                                "result": "Fail"
                            }]
                        }]
                    }]
                }]
            }
        };

        self.modifyRowData(self.resultsData.sequenceConfig.stepList);
        self.availableRows = ko.observableArray(self.resultsData.sequenceConfig.stepList);

        // Create bounded wrappers for the callbacks so they execute in the scope of this (self) object.

        callbackSuccess = lib.createBoundedWrapper(this, this.getJSON);
        callbackFail = lib.createBoundedWrapper(this, this.getJSONFailed);

        lib.getJSON(self.definition(), callbackSuccess, callbackFail);
    }

    /**
     * Declare the observable properties for the page.
     * @function declareProperties
     * @memberOf sequence.SequencePage
     * @todo Finish implementation.
     */
    SequencePage.prototype.declareProperties = function (data) {
        var self;

        self = this;
        self.name = ko.observable(data.name);
        self.summary = ko.observable(data.description.summary);
        self.description = ko.observable(data.description.detailed);
        self.definition = ko.observable(data.definition);
        self.endDate = ko.observable(data.endTimestamp);
        self.startDate = ko.observable(data.startTimestamp);
        self.status = ko.observable(data.status);

        self.unknown = "(not yet implemented)";

        self.statusStyle = ko.observable("text-" + lib.getStatusStyle(data.status));
    };

    /**
     * Update the observable properties for the page.
     * @function updateProperties
     * @memberOf sequence.SequencePage
     * @todo Finish implementation.
     */
    SequencePage.prototype.updateProperties = function (data) {
        var self;

        self = this;
        self.name(data.name);
        self.summary(data.description.summary);
        self.description(data.description.detailed);
        self.definition = ko.observable(data.definition);
        self.endDate(data.endTimestamp);
        self.startDate(data.startTimestamp);
        self.status(data.status);

        self.statusStyle("text-" + lib.getStatusStyle(data.status));
    };

    // Currently there is no drill down from this page...

    //SequencePage.prototype.goToRow = function (row)
    //{
    //    data.getData().sequence = row;
    //    location.hash = "sequence";
    //};


    /**
     * Modify the row (grid) data shown on the page.
     * @function modifyRowData
     * @memberOf sequence.SequencePage
     * @todo Finish implementation.
     */
    SequencePage.prototype.modifyRowData = function (rowData) {
        var i;
        var j;
        var style;
        var stepList;
        var valueList;

        // TODO: Calculate status from value and rules file (Frank Sep 09, 2014)

        stepList = rowData;
        console.log(stepList);

        for (i = 0; i < stepList.length; i++) {
            valueList = stepList[i].valueList;

            for (j = 0; j < valueList.length; j++) {
                if (j % 3 === 0) {
                    valueList[j].status = 'pass';
                } else if (j % 3 === 1) {
                    valueList[j].status = 'fail';
                } else {
                    valueList[j].status = 'other';
                }

                // Use a CSS style to color the status field.

                style = lib.getStatusStyle(valueList[j].status);
                valueList[j].statusStyle = style;
            }
        }
    };


    /**
     * Handle test results and the contained value(s) data.
     * @function getJSON
     * @memberOf sequence.SequencePage
     * @param data - data read from JSON file.
     * @todo Finish implementation.
     */
    SequencePage.prototype.getJSON = function (data) {
        try {
            var self;

            self = this;

            console.log(data.seriesInfo);

            self.updateProperties(data.seriesInfo);

            self.modifyRowData(data.sequence);

            // Update the model so the view is updated.

            self.availableRows(data.sequence);

        } catch (e) {
            logger.logError(e.message, e.lineNumber, e.fileNamesource, true);
        }

    };


    /**
     * Handle error when reading test results and the contained value(s) data.
     * @function getJSONFailed
     * @memberOf sequence.SequencePage
     * @param errorThrown - description of the error.
     * @todo Finish implementation.
     */
    SequencePage.prototype.getJSONFailed = function (errorThrown) {
        try {
            var self;

            self = this;

            console.log(errorThrown);
            // TODO: enable later (Frank 05Sep2014)
            //self.availableRows.removeAll();

        } catch (e) {
            logger.logError(e.message, e.lineNumber, e.fileNamesource, true);
        }

    };


    // This runs when the component is torn down. Put here any logic necessary to clean up,
    // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
    SequencePage.prototype.dispose = function () { };

    return { viewModel: SequencePage, template: templateMarkup };

});
