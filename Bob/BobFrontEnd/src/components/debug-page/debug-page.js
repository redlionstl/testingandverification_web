//----------------------------------------------------------------------------
// Copyright(c) 2014 Red Lion. All rights reserved.
//----------------------------------------------------------------------------
//
//   Creator: Frank Spafford
//   Created: 9/26/2014  16:16
//
// Description:
// ------------
/**
 This is the JSDoc namespace used for the debugging.
 The debug page is a place to try something. It is for debugging, and will not
 be in the released version of the Inspector.
 The code here is probably exempt from coding guidelines.
 @todo Remove for the released version.
 @namespace debug
*/
//----------------------------------------------------------------------------

/*global define*/


define(['knockout', 'text!./debug-page.html', 'logger', 'data'], function (ko, templateMarkup, logger, data) {

    "use strict"; // Defines that JavaScript code should be executed in "strict mode".

    var self;
    var vm; // vm - view model

    self = this;

    function DebugPage(params) {
        vm = this;
        this.message = ko.observable('Hello from the debug-page component!');
        this.contents = ko.observable('File contents...');
        console.log(params);
    }

    // This runs when the component is torn down. Put here any logic necessary to clean up,
    // for example cancelling setTimeouts or disposing Knockout subscriptions/computeds.
    DebugPage.prototype.dispose = function () { };

    // This method is used to upload a file. Created to test uploading selected JSON files.
    DebugPage.prototype.uploadFile = function (file) {
        var obj;
        var reader;

        console.log(file);
        console.log("Filename: " + file.name);
        console.log("Type: " + file.type);    // "Type: application/json"
        console.log("Size: " + file.size + " bytes");

        // TODO: Create a lib routine that will read the file object, parse the json, verify the name matches what is expected, and log errors and info. (Frank 22Aug2014)
        // (lib.getJSON does some of the above...)

        // Is this a JSON file?
        //if (!file || !file.type.match(/image.*/)) return;

        reader = new FileReader();

        reader.onload = function (event) {
            console.log(event);
            var contents = event.target.result;
            console.log("File contents: " + contents);

            // Show contents on page.
            vm.contents(contents);

            // Save for results view
            data.getData().sweeps = contents;

            try {
                obj = JSON.parse(contents);
            } catch (e) {
                alert(e); //error in the above string(in this case,yes)!
                logger.logError(e.message, e.lineNumber, e.fileNamesource, true);
            }

            console.log(typeof obj);
            if (typeof obj !== 'undefined') {
                console.log(obj);
                console.log(obj.SweepList);
                data.getData().sweeps = obj.SweepList;
                console.log(data.getData().sweeps);
                //logger.log("Read " + obj.SweepList.length + " objects", obj.SweepList.length, arguments.callee, 1);
                logger.log("Read " + obj.SweepList.length + " objects", obj.SweepList.length, "uploadFile", 1);
            }
        };

        reader.onerror = function (event) {
            console.error("File could not be read! Code " + event.target.error.code);
        };

        reader.readAsText(file);
    };

    DebugPage.prototype.resetFile = function () {
        console.log("resetFile", data.getData().sweeps);
        data.getData().sweeps = null;
        console.log("resetFile", data.getData().sweeps);
    };

    // Note: The "slash slash at-if..." lines below show how to use conditional compilation (inclusion). (Frank Sep 26, 2014)

    // @if NODE_ENV='debug'
    DebugPage.prototype.doSomething = function () {
        this.message('You invoked doSomething() on the viewmodel.');
        logger.log("informational message", "data 1 2 3", "home-page", 1);
        logger.logSuccess("success message", "data 1 2 3", "home-page", 1);
    };
    // @endif

    // @if NODE_ENV='debug'
    DebugPage.prototype.doSomething2 = function () {
        this.message('You invoked doSomething2() on the viewmodel.');
        logger.logError("error message", "data 1 2 3", "home-page", 1);
        logger.logWarning("warning message", "data 1 2 3", "home-page", 1);
    };
    // @endif

    return { viewModel: DebugPage, template: templateMarkup };
});
