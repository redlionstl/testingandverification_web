//----------------------------------------------------------------------------
// Copyright(c) 2014 Red Lion. All rights reserved.
//----------------------------------------------------------------------------
//
//   Creator: Frank Spafford
//   Created: 8/20/2014  14:40
//
// Description:
// ------------
// View model for the Sweep page.
//----------------------------------------------------------------------------

/**
 This is the JSDoc namespace used for the sweep view.
 @namespace sweep
*/

// http://lint-explain.herokuapp.com/a-is-defined-but-never-used/
/*jshint unused: vars */

define(['knockout', 'text!./sweep-page.html', 'logger', 'data', 'lib'], function (ko, templateMarkup, logger, data, lib) {

    "use strict"; // Defines that JavaScript code should be executed in "strict mode".

    /**
     Sweep view model
     @class SweepPage
     @memberOf sweep
     @todo Cleanup proof-of-concept code.
     @todo Read JSON files based on known file and folder names.
     */
    function SweepPage(params) {
        var callbackSuccess;
        var callbackFail;
        var callbackResultsSuccess;
        var callbackResultsFail;
        var self;
        var sweep;

        self = this;

        // TODO: Read sweepResults.json (Frank 15Sep2014)

        // Start with the data saved by the previous page, the row data for the drill down.
        sweep = data.getData().sweep;
        console.log(sweep);

        self.declareProperties(sweep);

        // TODO: Proof-of-concept fake data (will be disabled or removed): (Frank 25Sep2014)
        self.suiteData = [
	    {
	        "name": "SuiteOne",
	        "description": {
	            "summary": "Verifies basic BOB functionality",
	            "detailed": "This suite is part of proving out of the new Linux based test system more commonly knowns as B.O.B.  This is a very basic test suite that consistes of loging on to a device changing the location of the device then logging out.  Doing these simple functions exercise, and therefor prove working, all the major components and abstraction of the system"
	        },
	        "definition": "somepath/testsuite.json", // TODO:  (Frank 15Sep2014)
	        "theory": "The entire system is based on inheritance of generic and abstract base classes.  Therefore, successful  execution of an instantiated suite for a device proves the design and functionality.  See the wiki for design and implementation details",
	        "setup": {
	            "instructions": "The device should be set to defaults (excect maybe IP address)"
	        },
	        "startTimestamp": "8/13/2015 13:02:45",
	        "endTimestamp": "8/13/2015 13:05:12",
	    },
	    {
	        "name": "SuiteTwo",
	        "description": {
	            "summary": "Verifies basic BOB functionality 2",
	            "detailed": "This suite xxx is part of proving out of the new Linux based test system more commonly knowns as B.O.B.  This is a very basic test suite that consistes of loging on to a device changing the location of the device then logging out.  Doing these simple functions exercise, and therefor prove working, all the major components and abstraction of the system"
	        },
	        "definition": "anotherpath/testsuite.json",// TODO:  (Frank 15Sep2014)
	        "theory": "The entire system is based on inheritance of generic and abstract base classes.  Therefore, successful  execution of an instantiated suite for a device proves the design and functionality.  See the wiki for design and implementation details",
	        "setup": {
	            "instructions": "The device should be set to defaults (excect maybe IP address)"
	        },
	        "startTimestamp": "8/13/2015 13:02:45",
	        "endTimestamp": "9/23/2015 13:05:12",
	    }
        ];

        self.modifyRowData(self.suiteData);

        self.availableRows = ko.observableArray(self.suiteData);

        // Create bounded wrappers for the callbacks so they execute in the scope of this (self) object.

        callbackSuccess = lib.createBoundedWrapper(this, this.getJSON);
        callbackFail = lib.createBoundedWrapper(this, this.getJSONFailed);

        lib.getJSON(self.definition(), callbackSuccess, callbackFail);

        // Read results.

        callbackResultsSuccess = lib.createBoundedWrapper(this, this.getResults);
        callbackResultsFail = lib.createBoundedWrapper(this, this.getResultsFailed);

        lib.getJSON('SweepResults.json', callbackResultsSuccess, callbackResultsFail);
    }

    /**
     Drill down (show the details for) the selected row.
     @function goTo
     @memberOf sweep.SweepPage
     @param rowData The data for the selected row.
     @todo Finish JSDoc
    */
    SweepPage.prototype.goTo = function (rowData) {
        data.getData().suite = rowData;
        location.hash = "suite";
    };

    // This runs when the component is torn down. Put here any logic necessary to clean up,
    // for example canceling setTimeouts or disposing Knockout subscriptions/computeds.
    SweepPage.prototype.dispose = function () { };

    /**
     Declare the observable properties for the page.
     @function declareProperties
     @memberOf sweep.SweepPage
     @todo Finish implementation.
     */
    SweepPage.prototype.declareProperties = function (data) {
        try {
            var self;

            self = this;
            self.name = ko.observable(data.name);
            self.summary = ko.observable(data.description.summary);
            self.description = ko.observable(data.description.detailed);
            self.definition = ko.observable(data.definition);
            self.endDate = ko.observable(data.endTimestamp);
            self.createDate = ko.observable(data.created);
            self.startDate = ko.observable(data.startTimestamp);
            self.status = ko.observable(data.status);
            self.productInformation = ko.observable(data.productInformation);
            // TODO: Handle the port selection array (Frank 12Sep2014)
            self.configPort = ko.observable(data.portSelection[0].configPort);

            self.unknown = "(not yet implemented)";

            self.statusStyle = "text-" + lib.getStatusStyle(data.status);

            self.statusStyle2 = "bg-" + lib.getStatusStyle(data.status);
        } catch (e) {
            logger.logError('Exception: ' + e.message, e.lineNumber, e.fileNamesource, true);
        }
    };

    /**
     Update the observable properties for the page.
     @function updateProperties
     @memberOf sweep.SweepPage
     @todo Finish implementation.
    */
    SweepPage.prototype.updateProperties = function (data) {
        try {
            var self;

            self = this;
            self.name(data.name);
            self.summary(data.description.summary);
            self.description(data.description.detailed);
            self.definition(data.definition);
            self.endDate(data.endTimestamp);
            self.createDate(data.created);
            self.startDate(data.startTimestamp);
            self.status(data.status);
            self.productInformation(data.productInformation);
            // TODO: Handle the array (Frank 12Sep2014)
            self.configPort(data.portSelection[0].configPort);

            self.unknown = "(not yet implemented)";

            self.statusStyle = "text-" + lib.getStatusStyle(data.status);

            self.statusStyle2 = "bg-" + lib.getStatusStyle(data.status);
        } catch (e) {
            logger.logError('Exception: ' + e.message, e.lineNumber, e.fileNamesource, true);
        }
    };

    /**
     Modify the row (grid) data shown on the page.
     @function modifyRowData
     @memberOf sweep.SweepPage
     @param rowData Data associated with a row of the grid.
     @todo Finish implementation.
     */
    SweepPage.prototype.modifyRowData = function (rowData) {
        var i;
        var style;

        // TODO: Read status from results file (Frank 18Aug2014)

        for (i = 0; i < rowData.length; i++) {
            if (i % 2 === 0) {
                rowData[i].status = 'pass';
            } else {
                rowData[i].status = 'fail';
            }
        }

        // Use a CSS style to color the status field.

        for (i = 0; i < rowData.length; i++) {
            style = lib.getStatusStyle(rowData[i].status);
            rowData[i].statusStyle = style;
            //rowData[i].info = "JSON file: " + rowData[i].definition;
        }
    };

    /**
     Handle sweep and the contained suite(s) data.
     @function getJSON
     @memberOf sweep.SweepPage
     @param data - data read from JSON file.
     @todo Finish implementation.
    */
    SweepPage.prototype.getJSON = function (data) {
        try {
            var self;

            self = this;

            self.updateProperties(data.sweepInfo);

            self.modifyRowData(data.suites);

            // Update the model so the view is updated.

            self.availableRows(data.suites);

        } catch (e) {
            logger.logError('Exception: ' + e.message, e.lineNumber, e.fileNamesource, true);
        }
    };

    /**
     Handle error when reading sweep and the contained suite(s) data.
     @function getJSONFailed
     @memberOf sweep.SweepPage
     @param errorThrown - description of the error.
     @todo Finish implementation.
    */
    SweepPage.prototype.getJSONFailed = function (errorThrown) {
        try {
            var self;

            self = this;

            console.log(errorThrown);
            // TODO: enable below (Frank 09Sep2014)
            //self.availableRows.removeAll();

        } catch (e) {
            logger.logError('Exception: ' + e.message, e.lineNumber, e.fileNamesource, true);
        }

    };

    /**
     Handle results data for sweep and the contained suite(s).
     @function getResults
     @memberOf sweep.SweepPage
     @param data - data read from JSON results file.
     @todo Finish implementation.
    */
    SweepPage.prototype.getResults = function (data) {
        try {
            var self;

            self = this;

            console.log(data.sweep);
            console.log(data.suiteResultsList);

        } catch (e) {
            logger.logError('Exception: ' + e.message, e.lineNumber, e.fileNamesource, true);
        }
    };

    /**
    Handle failure when reading results data for sweep and the contained suite(s).
    @function getResultsFailed
    @memberOf sweep.SweepPage
    @param errorThrown - description of the error.
    @todo Finish implementation.
   */
    SweepPage.prototype.getResultsFailed = function (errorThrown) {
        try {
            var self;

            self = this;

            console.log(errorThrown);
            self.setRowDataFailure(self.suiteData);

            // Update all rows by removing and adding back.
            self.availableRows.removeAll();
            self.availableRows(self.suiteData);
        } catch (e) {
            logger.logError('Exception: ' + e.message, e.lineNumber, e.fileNamesource, true);
        }

    };

    /**
    Indicate no results data for a suite.
    @function setRowDataFailure
    @memberOf sweep.SweepPage
    @param rowData Data associated with a row of the grid.
    @todo Finish implementation.
   */
    SweepPage.prototype.setRowDataFailure = function (rowData) {
        var i;
        var style;

        for (i = 0; i < rowData.length; i++) {
            rowData[i].status = 'fail (could not read the results file)';
            style = lib.getStatusStyle(rowData[i].status);
            rowData[i].statusStyle = style;
        }
    };

    return { viewModel: SweepPage, template: templateMarkup };
});
