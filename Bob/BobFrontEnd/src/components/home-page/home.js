// http://lint-explain.herokuapp.com/a-is-defined-but-never-used/
/*jshint unused: vars */

define(["knockout", "text!./home.html", "logger", "data"], function (ko, homeTemplate, logger, data)
{
    "use strict";

  function HomeViewModel(route) {
      this.message = ko.observable('Welcome to Red Lion Testing!');
      //console.log(data.getConstants);
      //console.log(data.getConstants());
      this.version = ko.observable(data.getConstants().version);
  }

  return { viewModel: HomeViewModel, template: homeTemplate };

});
