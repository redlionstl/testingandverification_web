//----------------------------------------------------------------------------
// Copyright(c) 2014 Red Lion. All rights reserved.
//----------------------------------------------------------------------------
//
//   Creator: Frank Spafford
//   Created: 8/21/2014
//
// Description:
// ------------
// View model for the Series page.
//----------------------------------------------------------------------------

/**
 This is the JSDoc namespace used for the series view, which shows series details and a list of sequences.
 @namespace series
 @see {@link http://requirejs.org/docs/api.html#defdep|Definition Functions with Dependencies}
*/

// http://lint-explain.herokuapp.com/a-is-defined-but-never-used/
/*jshint unused: vars */

define(['knockout', 'text!./series-page.html', 'data', 'lib'], function (ko, templateMarkup, data, lib) {

    "use strict"; // Defines that JavaScript code should be executed in "strict mode".

    /**
     Series view model
     @class SeriesPage
     @memberOf series
     @todo Cleanup proof-of-concept code.
     @todo Read JSON files based on known file and folder names.
     @todo Update JSON field names.
     @todo Proof-of-concept fake data (will be disabled or removed)
    */
    function SeriesPage(params) {
        var callbackFail;
        var callbackSuccess;
        var self;
        var series;

        self = this;

        // TODO: Read seriesResults.json (Frank 15Sep2014)

        // Start with the data saved by the previous page, the row data for the drill down.
        series = data.getData().series;
        console.log(series);

        self.declareProperties(series);

        // TODO: Proof-of-concept fake data (will be disabled or removed): (Frank 25Sep2014)
        self.sequenceData = [
        {
            "name": "Change Device Location",
            "description": {
                "summary": "Verifies changing of the device location string",
                "detailed": "This sequence validates that the system can log on to a device via a web browser, change a text field, validate that change then log off"
            },
            "definition": "somepath/test1.json",// TODO:  (Frank 15Sep2014)
            "status": "boo",
            "statusStyle": "warning",// TODO:  (Frank 15Sep2014)
            "startTimestamp": "7/13/2010 13:02:45",
            "endTimestamp": "9/23/2017 13:05:12",
        },
        {
            "name": "Change Device Location",
            "description": {
                "summary": "Verifies changing of the device location string",
                "detailed": "This sequence validates that the system can log on to a device via a web browser, change a text field, validate that change then log off"
            },
            "definition": "somepath/test2.json",// TODO:  (Frank 15Sep2014)
            "status": "boo2",
            "statusStyle": "warning",// TODO:  (Frank 15Sep2014)
            "startTimestamp": "7/13/2010 13:02:45",
            "endTimestamp": "9/23/2017 13:05:12",
        }
        ];

        self.modifyRowData(self.sequenceData);
        self.availableRows = ko.observableArray(self.sequenceData);

        // Create bounded wrappers for the callbacks so they execute in the scope of this (self) object.

        callbackSuccess = lib.createBoundedWrapper(this, this.getJSON);
        callbackFail = lib.createBoundedWrapper(this, this.getJSONFailed);

        // TODO: Read JSON file based on known file/folder names. (Frank 29Sep2014)
        lib.getJSON(self.definition(), callbackSuccess, callbackFail);
    }

    /**
     Declare the observable properties for the page.
     @function declareProperties
     @memberOf series.SeriesPage
     @todo Finish implementation.
     @param data Series data.
     */
    SeriesPage.prototype.declareProperties = function (data) {
        var self;

        self = this;
        self.name = ko.observable(data.name);
        self.summary = ko.observable(data.description.summary);
        self.description = ko.observable(data.description.detailed);
        self.definition = ko.observable(data.definition);
        self.endDate = ko.observable(data.endTimestamp);
        self.startDate = ko.observable(data.startTimestamp);
        self.status = ko.observable(data.status);
        self.configuration = ko.observable(data.configuration);

        self.unknown = "(not yet implemented)";

        self.statusStyle = ko.observable("text-" + lib.getStatusStyle(data.status));
    };

    /**
     Update the observable properties for the page.
     @function updateProperties
     @memberOf series.SeriesPage
     @todo Finish implementation.
     @param data Series data.
     */
    SeriesPage.prototype.updateProperties = function (data) {
        var self;

        self = this;
        self.name(data.name);
        self.summary(data.description.summary);
        self.description(data.description.detailed);
        self.definition(data.definition);
        self.status(data.status);
        self.endDate(data.endTimestamp);
        self.startDate(data.startTimestamp);
        self.configuration(data.configuration);

        self.statusStyle("text-" + lib.getStatusStyle(data.status));
    };

    /**
     Drill down (show the details for) the selected row.
     @function goTo
     @memberOf series.SeriesPage
     @param row The data for the selected row.
    */
    SeriesPage.prototype.goTo = function (row) {
        data.getData().sequence = row;

        location.hash = "sequence";
    };


     /**
     Modify the row (grid) data shown on the page.
     @function modifyRowData
     @memberOf series.SeriesPage
     @param rowData Data associated with a row of the grid.
     @todo Finish implementation.
     */
   SeriesPage.prototype.modifyRowData = function (rowData) {
       var i;
       var style;

        // TODO: Read status from results file (Frank 18Aug2014)

        for (i = 0; i < rowData.length; i++) {
            if (i % 2 === 0) {
                rowData[i].status = 'pass';
            } else {
                rowData[i].status = 'fail';
            }

            // Use a CSS style to color the status field.

            style = lib.getStatusStyle(rowData[i].status);
            rowData[i].statusStyle = style;
        }
    };


    /**
     Handle series and the contained sequence(s) data.
     @function getJSON
     @memberOf series.SeriesPage
     @param data - data read from JSON file.
     @todo Finish implementation.
    */
    SeriesPage.prototype.getJSON = function (data) {
        try {
            var self;

            self = this;

            self.updateProperties(data.seriesInfo);

            self.modifyRowData(data.sequence);

            // Update the model so the view is updated.

            self.availableRows(data.sequence);

        } catch (e) {
            logger.logError(e.message, e.lineNumber, e.fileNamesource, true);
        }
    };

    /**
     Handle error when reading series and the contained sequence(s) data.
     @function getJSONFailed
     @memberOf series.SeriesPage
     @param errorThrown - description of the error.
     @todo Finish implementation.
    */
   SeriesPage.prototype.getJSONFailed = function (errorThrown) {
        try {
            var self;

            self = this;

            console.log(errorThrown);
            // TODO: enable later (Frank 05Sep2014)
            //self.availableRows.removeAll();

        } catch (e) {
            logger.logError(e.message, e.lineNumber, e.fileNamesource, true);
        }
    };

    // This runs when the component is torn down. Put here any logic necessary to clean up,
    // for example canceling setTimeouts or disposing Knockout subscriptions/computeds.
    SeriesPage.prototype.dispose = function () { };

    return { viewModel: SeriesPage, template: templateMarkup };

});
