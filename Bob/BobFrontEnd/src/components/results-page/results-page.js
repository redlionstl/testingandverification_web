//----------------------------------------------------------------------------
// Copyright(c) 2014 Red Lion. All rights reserved.
//----------------------------------------------------------------------------
//
//   Creator: Frank Spafford
//   Created: 8/18/2014  13:57
//
// Description:
// ------------
// This results-page shows the list of test sweeps that have been run.
// Click a row to drill down to more detail.
//----------------------------------------------------------------------------

// http://lint-explain.herokuapp.com/a-is-defined-but-never-used/
/*jshint unused: vars */

/**
 This is the JSDoc namespace used for the top level view, currently named "sweeps".
 @namespace sweeps
*/

define(['knockout', 'knockout-mapping', 'text!./results-page.html', 'logger', 'data', 'lib'], function (ko, komapping, templateMarkup, logger, data, lib) {

    // The above line shows how to load ko.mapping, but is currently not used.
    "use strict"; // Defines that JavaScript code should be executed in "strict mode".

    /**
     Results-page module which contains the data model and related methods.
     @class ResultsPage
     @memberOf sweeps
     @version 0.1 pre-alpha
     @todo Finish JSDoc comments, cleanup proof-of-concept code, remove or disable test code, etc. (Frank 04Sep2014)
    */
    function ResultsPage(params) {
        var i;
        var self;
        var style;

        self = this;

        // TODO: Remove obsolete fields from the sweepArray. (Frank 15Sep2014)
        // Proof-of-concept fake data (will be disabled or removed):
        self.sweepArray = [
        {
            "name": "Sweep_700",
            "description": {
                "summary": "Validation of Phase I BOB functionality1",
                "detailed": "This sweep is part of proving out of the new Linux based test system more commonly knowns as B.O.B.  This very basic sweep exercises,the major components and abstraction of the system"
            },
            "definition": "/Sweep_700/Sweep_700_test_sweep.json",
            "created": "1:65 PM Wednesday, August 13, 2014",
            "startTimestamp": "8/13/2015 13:02:45",
            "endTimestamp": "8/13/2015 13:05:12",
            "productInformation": "700Series devices",
            "status": "pass",
            "portSelection": [
                {
                    "configPort": "A1"
                }]
        },
        {
            "name": "Sweep_NT24k_1.5",
            "description": {
                "summary": "Validation of Phase I BOB functionality2",
                "detailed": "This sweep is part of proving out of the new Linux based test system more commonly knowns as B.O.B.  This very basic sweep exercises,the major components and abstraction of the system"
            },
            "definition": "/Sweep_NT24k_1.5/Sweep_NT24k_1.5_test_sweep.json", // remove
            "created": "1:65 PM Wednesday, August 13, 2014",
            "startTimestamp": "8/13/2015 13:02:45",
            "endTimestamp": "8/13/2015 13:05:12",
            "productInformation": "NT24k devices",
            "status": "fail", // remove
            "portSelection": [
                {
                    "configPort": "A2"
                }]
        },
        {
            "name": "Sweep_790",
            "description": {
                "summary": "Validation of Phase I BOB functionality3",
                "detailed": "This sweep is part of proving out of the new Linux based test system more commonly knowns as B.O.B.  This very basic sweep exercises,the major components and abstraction of the system"
            },
            "created": "1:65 PM Wednesday, August 13, 2014",
            "startTimestamp": "8/13/2015 13:02:45",
            "endTimestamp": "8/13/2015 13:05:12",
            "productInformation": "700Series: 7900 devices",
            "status": "pass",
            "portSelection": [
                {
                    "configPort": "B1"
                }]
        },
        {
            "name": "Sweep_NT24k_1.7",
            "description": {
                "summary": "Validation of Phase I BOB functionality",
                "detailed": "This sweep is part of proving out of the new Linux based test system more commonly knowns as B.O.B.  This very basic sweep exercises,the major components and abstraction of the system"
            },
            "definition": "/Sweep_NT24k_1.5/Sweep_NT24k_1.5_test_sweep.json",
            "created": "1:65 PM Wednesday, August 13, 2014",
            "startTimestamp": "8/13/2015 13:02:45",
            "endTimestamp": "8/13/2015 13:05:12",
            "productInformation": "NT24k devices that are black",
            "status": "failed miserably",
            "portSelection": [
                {
                    "configPort": "C1"
                }]
        }
        ];
        console.log(self.sweepArray);

        // This is how Knockout binds the model (data) to the view (html page).
        self.availableSweeps = ko.observableArray(self.sweepArray);

        // TODO: Read status from results file (Frank 18Aug2014)
        self.sweeps = data.getData().sweeps;
        console.log(typeof self.sweeps);
        if (self.sweeps) {
            self.sweepArray = self.sweeps;
            console.log(self.sweepArray);

            self.availableSweeps = ko.observableArray(self.sweepArray);
        }

        // Use a CSS style to color the status field.
        for (i = 0; i < self.availableSweeps().length; i++) {
            style = lib.getStatusStyle(self.availableSweeps()[i].status);
            self.availableSweeps()[i].statusStyle = style;
        }
    }

    /**
     Drill down (show the details for) the selected row.
     @function goTo
     @memberOf sweeps.ResultsPage
     @param rowData The data for the selected row.
     @todo Finish JSDoc
    */
   ResultsPage.prototype.goTo = function (rowData) {
        console.log(rowData);

        data.getData().sweep = rowData;

        location.hash = "sweep";
    };

    // This runs when the component is torn down. Put here any logic necessary to clean up,
    // for example canceling setTimeouts or disposing Knockout subscriptions/computeds.
    ResultsPage.prototype.dispose = function () { };

    return { viewModel: ResultsPage, template: templateMarkup };

});
