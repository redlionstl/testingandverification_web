﻿// http://lint-explain.herokuapp.com/a-is-defined-but-never-used/
/*jshint unused: vars */

define(["knockout", "text!./about.html", "logger", "data"], function (ko, aboutTemplate, logger, data)
{
    "use strict";

    function AboutViewModel(route)
    {
        //this.message = ko.observable('Welcome to Red Lion Testing!');
        //console.log(data.getConstants);
        //console.log(data.getConstants());
        this.version = ko.observable(data.getConstants().version);
    }

    return { viewModel: AboutViewModel, template: aboutTemplate };

});