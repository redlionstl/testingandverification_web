//----------------------------------------------------------------------------
// Copyright(c) 2014 Red Lion. All rights reserved.
//----------------------------------------------------------------------------
//
//   Creator: Frank Spafford
//   Created: 9/30/2014  16:36
//
// Description:
// ------------
// See below. The initial version of this file was created by knockout scaffolding.
// Knockout scaffolding can be used to add new components.
//----------------------------------------------------------------------------

define(['jquery', 'knockout', './router', 'bootstrap', 'knockout-projections', 'toastr'],

    /**
     This is an application startup module, which is used from the root index.html web page.
     Knockout components are registered here. This includes web pages, also known as views.
     @exports startup
     @version proof-of-concept
     @todo Remove dependencies, such as knockout-projections, if not used.
    */
    function ($, ko, router, toastr) {

    // Components can be packaged as AMD modules, such as the following:
    ko.components.register('nav-bar', { require: 'components/nav-bar/nav-bar' });
    ko.components.register('home-page', { require: 'components/home-page/home' });

    // ... or for template-only components, you can just point to a .html file directly:
    //ko.components.register('about-page', {
    //  template: { require: 'text!components/about-page/about.html' }
    //});

    ko.components.register('about-page', { require: 'components/about-page/about' });

    ko.components.register('results-page', { require: 'components/results-page/results-page' });

    ko.components.register('sweep-page', { require: 'components/sweep-page/sweep-page' });

    ko.components.register('suite-page', { require: 'components/suite-page/suite-page' });

    ko.components.register('debug-page', { require: 'components/debug-page/debug-page' });

    ko.components.register('series-page', { require: 'components/series-page/series-page' });

    ko.components.register('sequence-page', { require: 'components/sequence-page/sequence-page' });

    // [Scaffolded component registrations will be inserted here. To retain this feature, don't remove this comment.]

    // Start the application
    ko.applyBindings({ route: router.currentRoute });

});
