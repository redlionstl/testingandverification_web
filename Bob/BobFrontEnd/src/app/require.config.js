/**
 Asynchronous Module Definition (AMD) API for JavaScript modules
 Require.js looks for the following global (require) when initializing.
 This (the require object) is actually in the global namespace, but I am not yet sure how to indicate this with JSDoc.
 These JavaScript modules are used in the Inspector.
 @namespace require
 @see {@link http://requirejs.org/docs/api.html|Also, you can define the config object as the global variable require before require.js is loaded}
 @see {@link http://requirejs.org/|RequireJS is a JavaScript file and module loader}
 @version proof-of-concept
 @todo Remove knockout-projections if not used.
*/
var require = {
    baseUrl: ".",
    paths: {
        "bootstrap":            "bower_modules/components-bootstrap/js/bootstrap.min",
        "crossroads":           "bower_modules/crossroads/dist/crossroads.min",
        "hasher":               "bower_modules/hasher/dist/js/hasher.min",
        "jquery":               "bower_modules/jquery/dist/jquery",
        "knockout":             "bower_modules/knockout/dist/knockout.debug",
        "knockout-projections": "bower_modules/knockout-projections/dist/knockout-projections",
        "knockout-mapping":     "bower_modules/knockout-mapping/knockout.mapping",
        "signals":              "bower_modules/js-signals/dist/signals.min",
        "text":                 "bower_modules/requirejs-text/text",
        "toastr":               "bower_modules/toastr/toastr",
        "logger":               "services/logger",
        "data":                 "services/data",
        "lib":                  "services/lib"
},
    shim: {
        "bootstrap": { deps: ["jquery"] }
    }
};
