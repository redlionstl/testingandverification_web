//----------------------------------------------------------------------------
// Copyright(c) 2014 Red Lion. All rights reserved.
//----------------------------------------------------------------------------
//
//   Creator: Frank Spafford
//   Created: 9/30/2014
//
// Description:
// ------------
// See below. The initial version of this file was created by knockout scaffolding.
//----------------------------------------------------------------------------

define(["knockout", "crossroads", "hasher", "logger"],

     /**
     This is the application router module. It is used to associate a view (web page) with
     a suffix on the URL.
     <p>
     Original Comments:
     <p>
    This module configures crossroads.js, a routing library. If you prefer, you
    can use any other routing library (or none at all) as Knockout is designed to
    compose cleanly with external libraries.
    <p>
    You *don't* have to follow the pattern established here (each route entry
    specifies a 'page', which is a Knockout component) - there's nothing built into
    Knockout that requires or even knows about this technique. It's just one of
    many possible ways of setting up client-side routes.
     @exports router
     @version proof-of-concept
     @requires crossroads
     @requires hasher
     @requires knockout
     @requires logger
     @todo Finish JSDoc.
     @todo Use approved logger.
    */
   function (ko, crossroads, hasher, logger) {

       "use strict"; // Defines that JavaScript code should be executed in "strict mode".

       return new Router({
           routes: [
			   { url: '',			params:	{ page: 'home-page' }	},
			   { url: 'about',		params:	{ page:	'about-page' } },
			   { url: 'results',	params:	{ page:	'results-page' } },
			   { url: 'sweep',		params:	{ page:	'sweep-page' } },
			   { url: 'suite',		params:	{ page:	'suite-page' } },
			   { url: 'series',		params:	{ page:	'series-page' }	},
			   { url: 'sequence',	params:	{ page:	'sequence-page'	} },
			   { url: 'debug',		params:	{ page:	'debug-page' } }
			   ]
       });

       function Router(config) {
           var currentRoute = this.currentRoute = ko.observable({});

           ko.utils.arrayForEach(config.routes, function (route) {
               crossroads.addRoute(route.url, function (requestParams) {
                   currentRoute(ko.utils.extend(requestParams, route.params));
                   console.log("route ");
                   console.log(route);
               });
           });

           // Detect invalid routes.
           crossroads.bypassed.add(function (request) {
               console.log("invalid route " + request.toString());
               logger.logError("invalid route " + request, "data 3 4 5", "router", 1); // fires only once for same request

           });
           crossroads.ignoreState = true; // allows repeated errors on the same request

           activateCrossroads();
       }

       function activateCrossroads() {
           function parseHash(newHash, oldHash) { crossroads.parse(newHash); oldHash = null; }
           crossroads.normalizeFn = crossroads.NORM_AS_OBJECT;
           hasher.initialized.add(parseHash);
           hasher.changed.add(parseHash);
           hasher.init();
       }
   });