﻿//----------------------------------------------------------------------------
// Copyright(c) 2014 Red Lion. All rights reserved.
//----------------------------------------------------------------------------
//
//   Creator: Frank Spafford
//   Created: 8/19/2014  10:43
//
// Description:
// ------------
// Lib module. See JSDoc comments below.
//----------------------------------------------------------------------------

// The next few lines are lint options.
/*global $*/
// http://lint-explain.herokuapp.com/a-is-defined-but-never-used/
/*jshint unused: vars */

define(['logger'],
        /**
         A module representing a library of routines that may be used in the application.
         @exports lib
         @version 0.1 pre-alpha
        */
    function (logger)
    {
        "use strict"; // Defines that JavaScript code should be executed in "strict mode".

        var exports = {
            getStatusStyle: getStatusStyle,
            getJSON: getJSON,
            createBoundedWrapper: createBoundedWrapper
        };

        return exports;

        /**
         Return a bootstrap style for a pass/fail/other status.
         @param status - pass/fail/other status string
         @return Bootstrap style for pass/fail/other status.
         @todo Handle more variations of status (Frank 24Sep2014)
        */
        function getStatusStyle(status)
        {
            var style;
            if (status === 'pass')
            {
                style = 'success';
            }
            else if (status === 'fail')
            {
                style = 'danger';
            }
            else
            {
                style = 'warning';
            }
            return style;
        }


        /**
         Read a JSON file from the server using jQuery.
         @param filePath - path to the JSON file on the server.
         @param successCallback - routine called after successfully reading JSON data.
         @param failCallback - routine called when read fails.
         @todo Re-enable this function, which was disabled for demo. (Frank 15Sep2014)
         @todo Finish/cleanup code for logging errors. (Frank 24Sep2014)
        */
        function getJSON(filePath, successCallback, failCallback)
        {

            // @if NODE_ENV='debug-disable-for-demo' 

            // Load file from server
            // The jQuery XMLHttpRequest (jqXHR) object. See http://api.jquery.com/jQuery.ajax/#jqXHR

            $.getJSON(filePath, function (data)
            {
                // Success
                logger.logSuccess("Read file " + filePath, filePath, "lib.getJSON", 1);
            })
            .done(function (data, textStatus, jqXHR)
            {
                // Called when done.
                 if (successCallback !== undefined)
                {
                    successCallback(data);
                }
           })
            .fail(function (jqXHR, textStatus, errorThrown)
            {
                // Called on error(s) detected.
                console.log(".getJSON fail");
                console.log(jqXHR, textStatus, errorThrown);
                logger.logWarning("Failed to read file " + filePath + " - " + errorThrown, textStatus, "lib.getJSON", 1);
                if (failCallback !== undefined)
                {
                    failCallback(errorThrown);
                }
            })
            //.always(function(data|jqXHR, textStatus, jqXHR|errorThrown)
            //{
            //    alert("complete");
            //})
            ;

            // @endif

        }

        /**
         Create a bounded wrapper for a routine. Useful in callbacks.
        Taken from:
        {@link http://alistapart.com/article/getoutbindingsituations}
        Used to create a function that is bound to an object. Useful in callbacks to lib.getJSON.
         @param object The object for the context of the call.
         @param method The method called on the object (object.method).
         @return Whatever the method returns.
        */
        function createBoundedWrapper(object, method)
        {
            return function ()
            {
                return method.apply(object, arguments);
            };
        }
    });
