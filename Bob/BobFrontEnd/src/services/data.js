//----------------------------------------------------------------------------
// Copyright(c) 2014 Red Lion. All rights reserved.
//----------------------------------------------------------------------------
//
//   Creator: Frank Spafford
//   Created: 8/19/2014  10:43
//
// Description:
// ------------
// Data module. See JSDoc comments below.
//----------------------------------------------------------------------------

define(
    /**
     This module exists to allow data sharing between modules without using global variables.
     The idea is here: {@link http://stackoverflow.com/questions/5608685/using-requirejs-how-do-i-pass-in-global-objects-or-singletons-around}
     @exports data
     @version 0.1 pre-alpha
    */
    function () {
        "use strict"; // Defines that JavaScript code should be executed in "strict mode".

        var data = {};
        var constants = {
            "version": "0.0.0.5" // The version of this inspector application.
        };

        var exports = {
            getData: getData,
            getConstants: getConstants
        };

        return exports;

        /**
         Returns a data object that can be shared between modules.
         @return data
        */
        function getData() {
            return data;
        }

        /**
         Returns a constants object that can be shared between modules.
         @return constants
        */
        function getConstants() {
            return constants;
        }

    });