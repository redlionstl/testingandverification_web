//----------------------------------------------------------------------------
// Copyright(c) 2014 Red Lion. All rights reserved.
//----------------------------------------------------------------------------
//
//   Creator: Frank Spafford
//   Created: 8/18/2014  11:30
//
// Description:
// ------------
// This module defines the logger, which may be used to log various types of messages to the console and
// optionally to the UI using toastr.
//----------------------------------------------------------------------------

define(['toastr'],
    /**
     Logger module with functions to log various types of messages.
     @exports logger
     @version 0.1 pre-alpha
     @todo Add a method to show a modal toast, with close button. (Frank 18Aug2014)
     @todo Finish JSDoc comments? (Frank 04Sep2014)
     @see {@link http://codeseven.github.io/toastr/demo.html|toastr demo}
     @see {@link http://log4javascript.org|log4javascript is a JavaScript logging framework based on the Java logging framework log4j.}
     @see {@link http://zachlendon.github.io/blog/2012/11/12/log4javascript-local-storage-custom-appender/|Log4Javascript - Quick Intro and a LocalStorage Custom Appender}    */
    function (toastr) {
        "use strict"; // Defines that JavaScript code should be executed in "strict mode".

        var exports = {
            /** Log an informational message. */
            log: log,
            /** Log an error message. */
            logError: logError,
            /** Log a success message. */
            logSuccess: logSuccess,
            /** Log a warning message. */
            logWarning: logWarning
        };
        toastr.options.positionClass = 'toast-bottom-right';
        toastr.options.backgroundpositionClass = 'toast-bottom-right';

        // TODO: Reduce timeOut for release. (Frank 03Sep2014)

        /**
         @todo Reduce timeOut for release. (Frank 03Sep2014)
         */
        toastr.options.timeOut = 10000; // milliseconds

        return exports;


        /**
         Log an informational message.
         @param message
         @param data
         @param source
         @param showToast
         @todo Finish JSDoc
        */
        function log(message, data, source, showToast)
        {
            logIt(message, data, source, showToast, 'info');
        }
        /**
         Log an error message.
         @param message - message to log
         @param data - data to log
         @param source
         @param showToast
         @todo Finish JSDoc
        */
        function logError(message, data, source, showToast)
        {
            logIt(message, data, source, showToast, 'error');
        }

        /**
         Log a success message.
         @param message
         @param data
         @param source
         @param showToast
         @todo Finish JSDoc
        */
        function logSuccess(message, data, source, showToast)
        {
            logIt(message, data, source, showToast, 'success');
        }

        /**
         Log a warning message.
         @param message
         @param data
         @param source
         @param showToast
         @todo Finish JSDoc
        */
        function logWarning(message, data, source, showToast)
        {
            logIt(message, data, source, showToast, 'warning');
        }

        /**
         Private method.
         @private
         */
        function logIt(message, data, source, showToast, toastType)
        {
            source = source ? '[' + source + '] ' : '';
            // TODO: Handle console not defined. (Frank 12Sep2014)
            if (data)
            {
                console.log(source, message, data);
            }
            else
            {
                console.log(source, message);
            }
            if (showToast)
            {
                if (toastType === 'error')
                {
                    toastr.error(message);
                }
                else if (toastType === 'warning')
                {
                    toastr.warning(message);
                }
                else if (toastType === 'success')
                {
                    toastr.success(message);
                }
                else if (toastType === 'info')
                {
                    toastr.info(message);
                }

            }

        }
    });