﻿![Bob](http://icons.iconarchive.com/icons/bo/dilbert/32/Bob-icon.png)

## BobFrontEnd notes

This file contains "markdown" format. If you don't already have a markdown viewer, use the __MarkView__ extension for Chrome.

### Background ###

This project is based on Steve Sanderson's talk below.

* [Architecting large Single Page Applications with Knockout.js (Steve Sanderson)](http://blog.stevensanderson.com/2014/06/11/architecting-large-single-page-applications-with-knockout-js)
* [Automating Your Front-end Workflow with Yeoman 1.0 (Addy Osmani)](http://www.sitepoint.com/automating-your-front-end-workflow-with-yeoman-1-0-addy-osmani/)

### Prerequisite Tools

For running build tasks:

 * Node.js [Node.js® is a platform built on Chrome's JavaScript runtime for easily building fast, scalable network applications.](http://nodejs.org/)

For using tools to add more components:

 * Yeoman (npm install -g yo)
 * Knockout generator for yeoman (npm install -g generator-ko)

### Folder Structure ###

    BobFrontEnd       ->  front end folder
    |- dist         ->  distribution files
    |- node_modules ->  3rd-party node libraries for various build/test tools
    |- src          ->  source files
       |- app           -> application files (startup, routing, AMD)
       |- bower_modules -> 3rd-party libraries for the web application
       |- components    -> web components, such as views (pages)
       |- jsdoc         -> JSDoc documentation
       |- services      -> services (data, library) used by web components
    |- test         ->  unit tests

I believe the node_modules can be trimmed down by installing some of the modules globally.

## BobFrontEnd Files ##

Files inside root folder.

 * __bower.json__ : Configuration for the  bower tool. Contains libraries used by the web application.
 * __package.json__ : Configuration for node. Contains node packages used by the build/test tools.
 * __gulpfile.js__ : Gulp tasks for "building" the output files.
 * __readme.md__: This readme file using markdown formatting.

## src Files ##

Files inside `/src` folder.

 * index.html : Uncompressed start page for debugging.


### To add a new component:

 * yo ko:component (a-new-name)-page
 * add to router.js
 * add to nav-bar.html, if appropriate

### Build / Test tools

 * [Yeoman - The web's scaffolding tool for modern webapps](http://yeoman.io/)
 * [Bower - A package manager for the web](http://bower.io/)
 * [Gulp - A streaming build system](http://gulpjs.com/)
 * [Task Runner Explorer - A task runner for Grunt and Gulp directly within Visual Studio 2013.](http://visualstudiogallery.msdn.microsoft.com/8e1b4368-4afb-467a-bc13-9650572db708)

### Lint

To check for TODOs in JavaScript files, use JSLint to save these as tasks, then check the Task List - Add-ins.
Note: I currently use Web Essentials (JSHint and JSCS) in Visual Studio, and JSHint in Atom.

JSCS [JavaScript Code Style](https://www.npmjs.org/package/jscs)
JSHint [JSHint is a community-driven tool to detect errors and potential problems in JavaScript code](http://www.jshint.com/about/)

(TBD: Document settings/rules used.)

### JSON Hierarchy (brief)

---
    Sweeps
	    Sweep
		    Suite
			    Series
				    Results

---

### Repository Notes

Not all required files are stored in the repository. This includes those that are downloaded by package managers, such as Bower or the Node package manager.

After you checkout the repository, follow these steps. From the command line in the TestingAndVerification_core\src\web\Bob\BobFrontEnd folder:

 1. npm install -g
 2. bower install

(TBD: Is it best to install the node packages globally?)
[If you require() a module in your code, then that means it's a dependency, and a part of your program. You need to install it locally in your program.](https://www.npmjs.org/doc/misc/npm-faq.html)

### Web Libraries

Brief descriptions:

* Crossroads [Crossroads.js is a routing library](http://millermedeiros.github.io/crossroads.js/)
* Hasher [Hasher is a set of JavaScript functions to control browser history](https://github.com/millermedeiros/hasher/)
* RequireJS [RequireJS is a JavaScript file and module loader.](http://requirejs.org/)
* Bootstrap [Bootstrap is the most popular HTML, CSS, and JS framework for developing responsive, mobile first projects on the web.](http://getbootstrap.com/)
* Knockout [Knockout is a JavaScript library that helps you to create rich, responsive display and editor user interfaces with a clean underlying data model. ](http://knockoutjs.com/documentation/introduction.html)
* toastr [toastr is a Javascript library for non-blocking notifications.](https://github.com/CodeSeven/toastr)

### Gulp tasks

* gulp jsdoc - build JSDoc documentation.
* gulp - run the default task, which preprocesses, minimizes, and combines web files.
